function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
    }
   /* var str = val.toString().replace(/(.*)\.(.{2})$/,'$1' + ',' + '$2');*/
    return val;
}

function changewil(val)
{
    var tester = new RegExp(/wilayah/i);
    var result = tester.test(val);
    var wilayah;
    
    if(result == true)
    {
        wilayah = val.replace(/wilayah/i,"Wil");
    }
    else{
        wilayah = val; 
    }
    
    return wilayah;
}

var datatable_nasional;

datatable_nasional = $("#dt_nasional").DataTable({
    ajax: {
        url: link+'welcome/source/data_nasional?of=datatable',
        dataSrc: 'data'
    },
    "columnDefs": [
        {
            "targets" : 0,
            "defaultContent" : "Nasional"
        },
        {
            "targets" : 1,
            "data": "total_awal_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 2,
            "data": "total_real_rab_lalu",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 3,
            "data": "total_extern_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 4,
            "data": "total_real_rab_ini",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 5,
            "data": "total_extern_rab_ini",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 6,
            "data": "total_real_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 7,
            "data": "total_extern_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 8,
            "data": "total_sisa_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 9,
            "data": "total_proyeksi_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 10,
            "data": "persen_jumlah_awal",
        },
        {
            "targets" : 11,
            "data": "persen_jumlah_real_lalu",
        },
        {
            "targets" : 12,
            "data": "persen_jumlah_real_ini",
        },
        {
            "targets" : 13,
            "data": "persen_jumlah_real",
        },
        {
            "targets" : 14,
            "data": "persen_jumlah_proyeksi",
        }
    ],
    "paging": false,
    "bInfo": false,
    "sort":false,
    "searching":false,
    responsive: true
});


var datatable_wilayah;

datatable_wilayah = $("#dt_wilayah").DataTable({
    ajax: {
        url: link+'welcome/source/data_wilayah?of=datatable',
        dataSrc: 'data'
    },
    "columnDefs": [
        {
            "targets" : 0,
            "data": "unit_kerja",
            "render":function(data,type,row){
                return changewil(data);
            }
        },
        {
            "targets" : 1,
            "data": "total_awal_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 2,
            "data": "total_real_rab_lalu",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 3,
            "data": "total_extern_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 4,
            "data": "total_real_rab_ini",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 5,
            "data": "total_extern_rab_ini",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 6,
            "data": "total_real_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 7,
            "data": "total_extern_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 8,
            "data": "total_sisa_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 9,
            "data": "total_proyeksi_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 10,
            "data": "persen_jumlah_awal",
        },
        {
            "targets" : 11,
            "data": "persen_jumlah_real_lalu",
        },
        {
            "targets" : 12,
            "data": "persen_jumlah_real_ini",
        },
        {
            "targets" : 13,
            "data": "persen_jumlah_real",
        },
        {
            "targets" : 14,
            "data": "persen_jumlah_proyeksi",
        }
    ],
    "paging": false,
    "bInfo": false,
    "sort":false,
    "searching":false,
});

var datatable_proyek;

datatable_proyek = $("#dt_proyek").DataTable({
    ajax: {
        url: link+'welcome/source_per_div',
        dataSrc: 'data'
    },
    "columnDefs": [
        {
            "targets": 0,
            "defaultContent": "<span class='number'></span>"
        },
        {
            "targets" : 1,
            "data": "unit_kerja",
            "render":function(data,type,row){
                return changewil(data);
            }
        },
        {
            "targets" : 2,
            "data": "proyek"
        },
        {
            "targets" : 3,
            "data": "total_awal_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 4,
            "data": "total_real_rab_lalu",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 5,
            "data": "total_extern_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 6,
            "data": "total_real_rab_ini",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 7,
            "data": "total_extern_rab_ini",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 8,
            "data": "total_real_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 9,
            "data": "total_extern_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 10,
            "data": "total_sisa_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 11,
            "data": "total_proyeksi_rab",
            "render":function(data,type,row){
                return commaSeparateNumber(data);
            }
        },
        {
            "targets" : 12,
            "data": "persen_jumlah_awal"
        },
        {
            "targets" : 13,
            "data": "persen_jumlah_real_lalu"
        },
        {
            "targets" : 14,
            "data": "persen_jumlah_real_ini"
        },
        {
            "targets" : 15,
            "data": "persen_jumlah_real"
        },
        {
            "targets" : 16,
            "data": "persen_jumlah_proyeksi"
        }
    ],
    "paging": false,
    "bInfo": false,
    "sort":false,
    "searching":false,
});

datatable_proyek.
    on('draw.dt', function (){
        var jml = $('#dt_proyek tr td').find('.number').length;
        $('#dt_proyek tr td').find('.number').each(function(i,cell){
           var no = i+1
           if(no == jml){
            $(cell).text('-');
           }else{
            $(cell).text(no);
           }
        })
    });