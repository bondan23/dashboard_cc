"use strict";

Highcharts.setOptions({
    lang: {
        thousandsSep: ','
    }
});

var graphic_wilayah;

var option_wilayah = {
    chart: {
      renderTo: 'gr_wilayah',
      type: 'column'
    },

    title: {
      text: 'Grafik Pendapatan Usaha (Wilayah)',
    },
    subtitle: {
        text: 'Source: SIM Cost Control - PT. Nindya Karya (Persero)'
    },
    xAxis: {
      categories: [{}],
      crosshair:true
    },
    yAxis: {
        labels:{
            formatter:function(){
                return this.value + ' (Milyard)'
            }  
        },
        min: 0,
        allowDecimals: true,
        title:{
            text:"Wilayah"  
        },
        stackLabels: {
            enabled: true,
            formatter: function () {
                var kode = "column";
                var series = this.axis.series;
                var sum = {};
                var block ={};
                var loop = series[0].xData.length;
                
                
                for (var i = 0; i < series.length; i++){
                    var index = series[i].stackKey;
                    var value = series[i].yData;
                    var summary = 0;
                    for(var x=0;x<loop;x++)
                    {
                        if(index == 'columnb')
                        {
                            if(block[x] == undefined)
                            {
                                block[x] = [value[x]];
                            }
                            else{
                                block[x].push(value[x])
                            }
                            sum[index] = block;
                        }
                        else{
                            if(sum[index] == undefined)
                            {
                                sum[index] = [value[x]];
                            }
                            else{
                                sum[index].push(value[x])
                            }
                        }
                        
                    }
                }
                
                var stack = kode+this.stack;
                var a = {};
                $.each(sum[stack],function(k,v){
                    if($.isArray(v) == true)
                    {
                        var total = 0;
                        $.each(v,function(key,val){
                            total+= val;
                        })
                    }
                    else{
                        return false;
                    }
                    
                    a[k] = total;
                });
                
                sum['columnb'] = a;
				
                //return Highcharts.numberFormat(sum[stack][this.x]/1000000000,2,',');
            },
            style: {
                fontWeight: 'bold',
                fontSize: '11px',
                color: 'black'
            }
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + Highcharts.numberFormat(this.y,0,'.','.') + '<br/>' +
                'Total: ' +  Highcharts.numberFormat(this.point.stackTotal,0,'.','.');
        }
    },
    plotOptions: {
      column: {
        stacking: 'normal'
      }
    },

    series:[{}]
}


$.getJSON(link+'welcome/source/data_wilayah?of=graphic',function(data){
    /*if(data.series == undefined)
    {
        alert('Data di periode ini belum tersedia,silahkan ganti periode..')
    }*/
   
    option_wilayah.series = data.series;
    option_wilayah.xAxis.categories = data.categories;
    graphic_wilayah = new Highcharts.Chart(option_wilayah);
    
})

var grafik2;
var options2 = {
        chart: {
            type: 'column',
            renderTo: 'container2',
        },
        title: {
            text: 'Grafik Pendapatan Usaha (Nasional)'
        },
        subtitle: {
            text: 'Source: SIM Cost Control - PT. Nindya Karya (Persero)'
        },
        xAxis: {
            categories: [
                'Rencana Awal',
                'Realisasi s/d Saat Ini',
                'Rencana Sisa',
            ],
            crosshair: true
        },
        yAxis: {
            /*labels:{
                formatter:function(){
                    return this.value + ' (Triliun)'
                }  
            },*/
            min: 0,
            title: {
                text: 'Nasional'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:[{}]
}


/*grafik2 = new Highcharts.Chart(options2);*/
/*$.getJSON(link+'welcome/source/graph2',function(data){
    options2.series = data.series;
    grafik2 = new Highcharts.Chart(options2);
})*/

var test = 
    {
        "series":[
            {
                "name":"Rencana Awal",
                "data":[
                    1,
                    2.97,
                    3,
                    4.82
                ],
                "stack":"a"
            },
            {
                "name":"Realisasi s\/d Saat Ini",
                "data":[
                    5,
                    6,
                    7,
                    8
                ],
                "stack":"b"
            },
            {
                "name":"Rencana Sisa",
                "data":[
                    9,
                    10.97,
                    11,
                    12.82
                ],
                "stack":"b"
            }
        ],
    };

var graphic_nasional;
var option_nasional = {
        chart: {
            type: 'column',
            renderTo: 'gr_nasional',
        },
        title: {
            text: 'Grafik Pendapatan Usaha (Nasional)'
        },
        subtitle: {
            text: 'Source: SIM Cost Control - PT. Nindya Karya (Persero)'
        },
        xAxis: {
            categories: [
                'Dashboard Nasional',
            ],
            crosshair: true
        },
        yAxis: {
            labels:{
                formatter:function(){
                    return this.value/1000 + ' (Milyard)'
                }  
            },
            min: 0,
            title: {
                text: 'Nasional'
            }
        },
        tooltip: {
            /*formatter:function(){

                return this.point.series.name + ': <b>'+Highcharts.numberFormat(this.point.y,2,'.',',')+'</b>';
            }*/
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:,.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true,
            /*positioner: function(boxWidth, boxHeight, point) {         
                return {x:point.plotX + 20,y:point.plotY};         
            }*/
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:[{}]
}

$.getJSON(link+'welcome/source/data_nasional?of=graphic',function(data){
    option_nasional.series = data.data;
    graphic_nasional = new Highcharts.Chart(option_nasional);
})


var graphic_proyek;
var option_proyek = {
        chart: {
            type: 'column',
            renderTo: 'gr_proyek',
        },
        title: {
            text: 'Grafik Pendapatan Usaha (Per-Proyek)'
        },
        subtitle: {
            text: 'Source: SIM Cost Control - PT. Nindya Karya (Persero)'
        },
        xAxis: {
            categories: [
                'Dashboard Proyek',
            ],
            crosshair: true
        },
        yAxis: {
            labels:{
                formatter:function(){
                    return this.value + ' (Milyard)'
                }  
            },
            min: 0,
            title: {
                text: 'Proyek'
            }
        },
        tooltip: {
            /*formatter:function(){

                return this.point.series.name + ': <b>'+Highcharts.numberFormat(this.point.y,2,'.',',')+'</b>';
            }*/
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:,.2f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true,
            /*positioner: function(boxWidth, boxHeight, point) {         
                return {x:point.plotX + 20,y:point.plotY};         
            }*/
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:[{}]
}

$.getJSON(link+'welcome/source_per_div?graphic=true',function(data){
    option_proyek.series = data.series;
    graphic_proyek = new Highcharts.Chart(option_proyek);
})

//grafik4 = new Highcharts.Chart(options4);
