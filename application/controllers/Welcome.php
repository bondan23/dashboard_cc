<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*use GuzzleHttp\Client as Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;*/

class Welcome extends CI_Controller {
    private $guzzle;
    
    private $version = '1.5.0';
    public  $data = array();
    private $apikey = "964b9197-e11e-4c3b-b03a-51d4a8cdeacf";
    private $divided = 1000;
    private $ip_whitelist = array(
        '::1', //for developing
        '127.0.0.1', //for developing
        '192.168.11.47', //produksi pusat
        '192.168.47.58', //produksi surabaya
        '192.168.51.8', //ksc makassar
        '192.168.16.50', //ip laptop zenbook
        '192.168.16.26', //ip pc pak andri
        '192.168.16.46', //ip macbook pak andri
        '192.168.71.10', //wil 1 medan
        '192.168.36.10', //wil 2 palembang
        '192.168.47.69', //wil 4 surabaya
        '192.168.41.10', //wil 3 balikpapan
        '192.168.57.100', //wil 6 jakarta
    );
    
    //@param $refresh_in_minutes (int)
    private $refresh_in_minutes = 5;
    
    public function __construct()
    {
        parent::__construct();
        //  $this->guzzle = new Client;
        $this->output->set_version($this->version);
        $this->data['version'] = $this->version;
        require_once APPPATH."libraries/Requests.php";
        $this->auth();
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
    
    private function auth()
    {
        $ip = $this->input->server('REMOTE_ADDR');
        $cookie = @$_COOKIE['cusername'];
        $check = in_array($ip,$this->ip_whitelist);
        if($check == true || isset($cookie))
        {
            return true;
        }
        else{
            redirect('../');
        }
    }
    
    public function dashboard()
    {
        $this->output->set_template('default');
        $this->output->set_title('.:: PT.Nindya Karya (Persero)');
        $this->output->append_title('Dashboard SIMCC ::.');
        
        $this->get_dropdown_periode();
        
        $wil = $this->get_divisi();
        $this->data['divisi']['ALL'] = "ALL";
        foreach($wil as $v)
        {
            if($v['urut'] <= 8 && $v['urut'] != null){
                $this->data['divisi'][$v['divisi_kode']] = $v['divisi_name'];
            }
        }
        
        asort($this->data['divisi']);
        
        //inject waktu refresh
        $this->data['menit'] = $this->refresh_in_minutes;
        
        $this->load->view('dashboard',$this->data);
    }
    
    public function tester(){
        require_once APPPATH."libraries/Requests.php";
        
        $local = "http://simnk.nindyakarya.co.id/simcc/home/get_konsolidasi?periode_bulan=2016081&divisi_kode=ALL";
        $apikey = "ab2921-0sad78-sa9898as-22jjj";
        
        // Next, make sure Requests can load internal classes
        Requests::register_autoloader();

        // Now let's make a request!
        $request = Requests::get($local, array('Authorization' => 'Basic '.$apikey,'Accept' => 'application/json'));

        // Check what we received
        var_dump($request);
    }
    
    private function get_divisi()
    {
        $local = "http://simnk.nindyakarya.co.id/simcc/apisync/service/get_wilayah";
        try{
            // Next, make sure Requests can load internal classes
            Requests::register_autoloader();

            // Now let's make a request!
            $request = Requests::get($local, array('Authorization' => 'Basic '.$this->apikey,'Accept' => 'application/json'));
            
            $json = json_decode($request->body,1);
            
            return $json['data'];
        }
        catch(Exception $e)
        {
            die('die');
        }
    }
    
    private function get_periode_konsolidasi()
    {
        $local = "http://simnk.nindyakarya.co.id/simcc/apisync/service/get_periode_konsolidasi";
        try{
            // Next, make sure Requests can load internal classes
            Requests::register_autoloader();

            // Now let's make a request!
            $request = Requests::get($local, array('Authorization' => 'Basic '.$this->apikey,'Accept' => 'application/json'));
            
            $json = json_decode($request->body,1);
            
            return $json['data'];
        }
        catch(Exception $e)
        {
            die('die');
        }
    }
    
    private function get_dropdown_periode()
    {
        $data = $this->get_periode_konsolidasi();
        
        foreach($data as $k=>$v)
        {
            if($v['selected'] == true)
            {
                $selected = $data[$k]['bulan_minggu'];
            }
            
            $dropdown[$v['bulan_minggu']] = $v['text_periode'];
        }
        
        $this->data['dropdown_periode'] = $dropdown;
        $this->data['dropdown_selected'] = $selected;
    }
    
    /*
    *
    * Source Data Untuk Graphic Per-Proyek
    * param : $div (String), $periode (Integer)
    * return : json
    *
    */
    public function source_per_div($div="ALL",$periode=null)
    {
        if($periode == null)
        {
            $local = "http://simnk.nindyakarya.co.id/simcc/home/get_konsolidasi_baru?divisi_kode=".strtoupper($div);
        }
        else{
            $local = "http://simnk.nindyakarya.co.id/simcc/home/get_konsolidasi_baru?divisi_kode=".strtoupper($div)."&periode_bulan=".$periode;
        }
        
        try{
            // Next, make sure Requests can load internal classes
            Requests::register_autoloader();

            // Now let's make a request!
            $request = Requests::get($local, array('Authorization' => 'Basic '.$this->apikey,'Accept' => 'application/json'));
            
            $json = json_decode($request->body,1);
            $output = $json['data'];
            
            if(empty($output))
            {
                $msg = '{"status":false,"data":null,"msg":"un-available"}';
                throw new Exception($msg);   
            }
            
            
            /*UTILITY*/
            $open = "<b>";
            $close = "</b>";
            
            /*CLOSURE*/
            $number_format = function($data,$tmp = '%.0f'){
                $format = sprintf($tmp,$data);
                return floatval($format);
            };
            
            $total_format = function($data,$graph=false) use ($number_format){
                $value = array_sum($data);
                
                if($graph == false):
                    return "<b>".$number_format($value)."</b>";
                else:
                    return $number_format($value);
                endif;
            };
            
            $persen_format = function($data,$tmp = '%.2f'){
                 $format = sprintf($tmp,$data);
                 return floatval($format);
            };
            
            $total_persen = function($data,$divided_by,$graph=false) use ($persen_format){
                $value = array_sum($data)/$divided_by;
                
                if($graph == false):
                    return "<b>".$persen_format($value)."</b>";
                else:
                    return $persen_format($value);
                endif;
            };
            
            
            /*Remake array*/
            $offset=0;
            foreach($output as $v)
            {
                $data[] = array(
                    'unit_kerja'=>" ".$v['divisi_name'],
                    'proyek'=>$v['proyek'],
                    'total_awal_rab'=>$number_format($v['total_awal_rab']),
                    'total_real_rab_lalu'=>$number_format($v['total_real_rab_lalu']),
                    'total_extern_rab'=>$number_format($v['total_extern_rab']),
                    'total_real_rab_ini'=>$number_format($v['total_real_rab_ini']),
                    'total_extern_rab_ini'=>$number_format($v['total_extern_rab_ini']),
                    'total_real_rab'=>$number_format($v['total_real_rab']),
                    'total_extern_rab'=>$number_format($v['total_extern_rab']),
                    'total_sisa_rab'=>$number_format($v['total_sisa_rab']),
                    'total_proyeksi_rab'=>$number_format($v['total_proyeksi_rab']),
                    'jumlah_awal'=>$number_format($v['jumlah_awal']),
                    'jumlah_real_lalu'=>$number_format($v['jumlah_real_lalu']),
                    'jumlah_real_ini'=>$number_format($v['jumlah_real_ini']),
                    'jumlah_real'=>$number_format($v['jumlah_real']),
                    'jumlah_proyeksi'=>$number_format($v['jumlah_proyeksi']),
                    'persen_jumlah_awal'=>$persen_format($v['persen_jumlah_awal']),
                    'persen_jumlah_real_lalu'=>$persen_format($v['persen_jumlah_real_lalu']),
                    'persen_jumlah_real_ini'=>$persen_format($v['persen_jumlah_real_ini']),
                    'persen_jumlah_real'=>$persen_format($v['persen_jumlah_real']),
                    'persen_jumlah_sisa'=>$persen_format($v['persen_jumlah_sisa']),
                    'persen_jumlah_proyeksi'=>$persen_format($v['persen_jumlah_proyeksi'])
                );
                
                
                $divisi[$v['urut']]  = $v['divisi_name'];
                
                /*TOTAL*/
                $total_awal_rab[$offset] = $v['total_awal_rab'];
                $total_real_rab_lalu[$offset] = $v['total_real_rab_lalu'];
                $total_extern_rab[$offset] = $v['total_extern_rab'];
                $total_real_rab_ini[$offset] = $v['total_real_rab_ini'];
                $total_extern_rab_ini[$offset] = $v['total_extern_rab_ini'];
                $total_real_rab[$offset] = $v['total_real_rab'];
                $total_extern_rab[$offset] = $v['total_extern_rab'];
                $total_sisa_rab[$offset] = $v['total_sisa_rab'];
                $total_proyeksi_rab[$offset] = $v['total_proyeksi_rab'];
                $jumlah_awal[$offset] = $v['jumlah_awal'];
                $jumlah_real_lalu[$offset] = $v['jumlah_real_lalu'];
                $jumlah_real_ini[$offset] = $v['jumlah_real_ini'];
                $jumlah_real[$offset] = $v['jumlah_real'];
                $jumlah_proyeksi[$offset] = $v['jumlah_proyeksi'];
                
                /*PERSEN*/
                $total_persen_jumlah_awal[$v['divisi_name']][$offset] = $v['persen_jumlah_awal'];
                $total_persen_jumlah_real_lalu[$v['divisi_name']][$offset] = $v['persen_jumlah_real_lalu'];
                $total_persen_jumlah_real_ini[$v['divisi_name']][$offset] = $v['persen_jumlah_real_ini'];
                $total_persen_jumlah_real[$v['divisi_name']][$offset] = $v['persen_jumlah_real'];
                $total_persen_jumlah_proyeksi[$v['divisi_name']][$offset] = $v['persen_jumlah_proyeksi'];
                /*PERSEN*/
                
                    /*LABA*/
                        $rencana_laba[$offset] = $v['total_awal_rab']-$v['jumlah_awal'];
                        $realisasi_laba[$offset] = $v['total_real_rab']-$v['jumlah_real'];
                        $proyeksi_laba[$offset] = $v['total_proyeksi_rab']-$v['jumlah_proyeksi'];
                    /*LABA*/
                
                /*TOTAL*/
                $offset++;
                
            }
            //variable pengecek output data
            $graphic = $this->input->get('graphic');
            
            
            if($graphic == false){
                
                /*REMAKE ARRAY PERSEN*/
                /*JIKA ADA VARIABLE BARU DI DALAM COMMENT PERSEN,TAMBAH DISINI*/
                $array_list = array(
                    '$total_persen_jumlah_awal',
                    '$total_persen_jumlah_real_lalu',
                    '$total_persen_jumlah_real_ini',
                    '$total_persen_jumlah_real',
                    '$total_persen_jumlah_proyeksi'
                );
                
                /*REMAKE ARRAY*/
                foreach($array_list as $v)
                {
                    $str = substr($v,1);
                    eval("\$new_array[@$str] = $v;");
                    
                    foreach($new_array[$str] as $key=>$val)
                    {
                        eval("\$new[@$str][] =".array_sum($val)/count($val).";");
                    }
                    
                    //overwrite variable yg ada di arraylist dengan nilai baru
                    eval("\$$str =".array_sum($new[$str])/count($new[$str]).";");
                    
                }
                
                //echo $new_total_persen_jumlah_awal;
                
                //debugger($new_array);
                //debugger($new_total_persen_jumlah_awal);
                
                /*DATATABLE*/
                $hitung = count($data);
                $data[$hitung] = array(
                    'unit_kerja'=>"<b>Total</b>",
                    'proyek'=>"<b>Total</b>",
                    'total_awal_rab'=>$total_format($total_awal_rab),
                    'total_real_rab_lalu'=>$total_format($total_real_rab_lalu),
                    'total_extern_rab'=>$total_format($total_extern_rab),
                    'total_real_rab_ini'=>$total_format($total_real_rab_ini),
                    'total_extern_rab_ini'=>$total_format($total_extern_rab_ini),
                    'total_real_rab'=>$total_format($total_real_rab),
                    'total_extern_rab'=>$total_format($total_extern_rab),
                    'total_sisa_rab'=>$total_format($total_sisa_rab),
                    'total_proyeksi_rab'=>$total_format($total_proyeksi_rab),
                    'jumlah_awal'=>$total_format($jumlah_awal),
                    'jumlah_real_lalu'=>$total_format($jumlah_real_lalu),
                    'jumlah_real_ini'=>$total_format($jumlah_real_ini),
                    'jumlah_real'=>$total_format($jumlah_real),
                    'jumlah_proyeksi'=>$total_format($jumlah_proyeksi),
                    'persen_jumlah_awal'=>$open.$persen_format($total_persen_jumlah_awal).$close,
                    'persen_jumlah_real_lalu'=>$open.$persen_format($total_persen_jumlah_real_lalu).$close,
                    'persen_jumlah_real_ini'=>$open.$persen_format($total_persen_jumlah_real_ini).$close,
                    'persen_jumlah_real'=>$open.$persen_format($total_persen_jumlah_real).$close,
                    'persen_jumlah_proyeksi'=>$open.$persen_format($total_persen_jumlah_proyeksi).$close,
                );
                /*DATATABLE*/ 
                
                //debugger($divisi);
                //debugger($total_persen_jumlah_awal);
               
                echo json_encode(array("data"=>$data));
            }
            else{
                $series = array(
                    'total_awal_rab'=>$total_format($total_awal_rab,true),
                    'total_real_rab_lalu'=>$total_format($total_real_rab_lalu,true),
                    'total_extern_rab'=>$total_format($total_extern_rab,true),
                    'total_real_rab_ini'=>$total_format($total_real_rab_ini,true),
                    'total_extern_rab_ini'=>$total_format($total_extern_rab_ini,true),
                    'total_real_rab'=>$total_format($total_real_rab,true),
                    'total_extern_rab'=>$total_format($total_extern_rab,true),
                    'total_sisa_rab'=>$total_format($total_sisa_rab,true),
                    'total_proyeksi_rab'=>$total_format($total_proyeksi_rab,true),
                    'jumlah_awal'=>$total_format($jumlah_awal,true),
                    'jumlah_real_lalu'=>$total_format($jumlah_real_lalu,true),
                    'jumlah_real_ini'=>$total_format($jumlah_real_ini,true),
                    'jumlah_real'=>$total_format($jumlah_real,true),
                    'jumlah_proyeksi'=>$total_format($jumlah_proyeksi,true),
                    'rencana_laba'=>$total_format($rencana_laba,true),
                    'realisasi_laba'=>$total_format($realisasi_laba,true),
                    'proyeksi_laba'=>$total_format($proyeksi_laba,true)
                );
                
                $graph_name = array(
                    "total_awal_rab"=>"Nilai Kontrak",
                    "total_real_rab_ini"=>"Realisasi Aktual",
                    "total_extern_rab_ini"=>"Realisasi Diakui",
                    "total_sisa_rab"=>"Sisa Pekerjaan",
                    "total_proyeksi_rab"=>"Proyeksi",
                    "rencana_laba"=>"Rencana (Laba)",
                    "realisasi_laba"=>"Realisasi (Laba)",
                    "proyeksi_laba"=>"Proyeksi (Laba)"
                );
                
                
                foreach($graph_name as $k=>$v)
                {
                    $graph[] = array(
                        "name"=>$v,
                        "data"=> array(
                            $series[$k]
                        )
                    );
                }
                
                echo json_encode(array("series"=>$graph));
            }
            
            //debugger($data);
        }
        catch(Exception $e)
        {
            $this->output->set_content_type('application/json')->set_output($e->getMessage());
        }
    }
    
    /*
    *
    * Source Data Untuk Graphic Nasional dan Wilayah
    * param : $tipe (String)
    * return : json
    *
    */
    public function source($tipe=null)
    {
        $periode = $this->input->get('periode_bulan') | null;
        
        if($periode == null)
        {
            $local = "http://simnk.nindyakarya.co.id/simcc/home/get_konsolidasi_baru?divisi_kode=ALL";
        }
        else{
            $local = "http://simnk.nindyakarya.co.id/simcc/home/get_konsolidasi_baru?divisi_kode=ALL&periode_bulan=".$periode;
        }
        
        try{
            // Next, make sure Requests can load internal classes
            Requests::register_autoloader();

            // Now let's make a request!
            $request = Requests::get($local, array('Authorization' => 'Basic '.$this->apikey,'Accept' => 'application/json'));
            
            $json = json_decode($request->body,1);
            $output = $json['data'];
            
            if(empty($output))
            {
                $msg = '{"status":false,"data":null,"msg":"un-available"}';
                throw new Exception($msg);   
            }
            
            /*Remake array*/
            foreach($output as $v)
            {
                $data[$v['divisi_name']]['total_awal_rab'][] = $v['total_awal_rab'];
                $data[$v['divisi_name']]['total_real_rab_lalu'][] = $v['total_real_rab_lalu'];
                $data[$v['divisi_name']]['total_extern_rab'][] = $v['total_extern_rab'];
                $data[$v['divisi_name']]['total_real_rab_ini'][] = $v['total_real_rab_ini'];
                $data[$v['divisi_name']]['total_extern_rab_ini'][] = $v['total_extern_rab_ini'];
                $data[$v['divisi_name']]['total_real_rab'][] = $v['total_real_rab'];
                $data[$v['divisi_name']]['total_extern_rab'][] = $v['total_extern_rab'];
                $data[$v['divisi_name']]['total_sisa_rab'][] = $v['total_sisa_rab'];
                $data[$v['divisi_name']]['total_proyeksi_rab'][] = $v['total_proyeksi_rab'];
                $data[$v['divisi_name']]['jumlah_awal'][] = $v['jumlah_awal'];
                $data[$v['divisi_name']]['jumlah_real_lalu'][] = $v['jumlah_real_lalu'];
                $data[$v['divisi_name']]['jumlah_real_ini'][] = $v['jumlah_real_ini'];
                $data[$v['divisi_name']]['jumlah_real'][] = $v['jumlah_real'];
                $data[$v['divisi_name']]['jumlah_proyeksi'][] = $v['jumlah_proyeksi'];
                
                /*PERSEN BKPU*/
                $data[$v['divisi_name']]['persen_jumlah_awal'][] = $v['persen_jumlah_awal'];
                $data[$v['divisi_name']]['persen_jumlah_real_lalu'][] = $v['persen_jumlah_real_lalu'];
                $data[$v['divisi_name']]['persen_jumlah_real_ini'][] = $v['persen_jumlah_real_ini'];
                $data[$v['divisi_name']]['persen_jumlah_real'][] = $v['persen_jumlah_real'];
                $data[$v['divisi_name']]['persen_jumlah_sisa'][] = $v['persen_jumlah_sisa'];
                $data[$v['divisi_name']]['persen_jumlah_proyeksi'][] = $v['persen_jumlah_proyeksi'];
                /*PERSEN BKPU*/
            }
            
            
            /*KEYS*/
            $keys = array_keys($data); //unit kerja tersedia
            $count_unit = count($keys); //hitung jumlah unit kerja
            
            /*CLOSURE*/
            $number_format = function($data,$tmp = '%.0f'){
                $format = sprintf($tmp,$data);
                return floatval($format);
            };
            
            $persen_format = function($data,$tmp = '%.2f'){
                 $format = sprintf($tmp,$data);
                 return floatval($format);
            };
            
            $total_format = function($data,$graph=false) use ($number_format){
                $value = array_sum($data);
                
                if($graph == false):
                    return "<b>".$number_format($value)."</b>";
                else:
                    return $number_format($value);
                endif;
            };
            
            $total_persen = function($data,$divided_by,$graph=false) use ($persen_format){
                $value = array_sum($data)/$divided_by;
                
                if($graph == false):
                    return "<b>".$persen_format($value)."</b>";
                else:
                    return $persen_format($value);
                endif;
            };
            
            if($tipe == 'data_nasional'){
                
                //tipe output format (of)
                $of = $this->input->get('of');
                
                $offset =0;
                foreach($keys as $v)
                {
                    $dt_nasional['total_awal_rab'][$offset] = array_sum($data[$v]['total_awal_rab']);
                    $dt_nasional['total_real_rab_lalu'][$offset] = array_sum($data[$v]['total_real_rab_lalu']);
                    $dt_nasional['total_extern_rab'][$offset] = array_sum($data[$v]['total_extern_rab']);
                    $dt_nasional['total_real_rab_ini'][$offset] = array_sum($data[$v]['total_real_rab_ini']);
                    $dt_nasional['total_extern_rab_ini'][$offset] = array_sum($data[$v]['total_extern_rab_ini']);
                    $dt_nasional['total_real_rab'][$offset] = array_sum($data[$v]['total_real_rab']);
                    $dt_nasional['total_extern_rab'][$offset] = array_sum($data[$v]['total_extern_rab']);
                    $dt_nasional['total_sisa_rab'][$offset] = array_sum($data[$v]['total_sisa_rab']);
                    $dt_nasional['total_proyeksi_rab'][$offset] = array_sum($data[$v]['total_proyeksi_rab']);
                    $dt_nasional['jumlah_awal'][$offset] = array_sum($data[$v]['jumlah_awal']);
                    $dt_nasional['jumlah_real_lalu'][$offset] = array_sum($data[$v]['jumlah_real_lalu']);
                    $dt_nasional['jumlah_real_ini'][$offset] = array_sum($data[$v]['jumlah_real_ini']);
                    $dt_nasional['jumlah_real'][$offset] = array_sum($data[$v]['jumlah_real']);
                    $dt_nasional['jumlah_proyeksi'][$offset] = array_sum($data[$v]['jumlah_proyeksi']);
                    
                    
                    
                    /*LABA*/
                    $dt_nasional['rencana_laba'][$offset] = array_sum($data[$v]['total_awal_rab'])-array_sum($data[$v]['jumlah_awal']);
                    $dt_nasional['realisasi_laba'][$offset] = array_sum($data[$v]['total_real_rab'])-array_sum($data[$v]['jumlah_real']);
                    $dt_nasional['proyeksi_laba'][$offset] = array_sum($data[$v]['total_proyeksi_rab'])-array_sum($data[$v]['jumlah_proyeksi']);
                    /*LABA*/
                    
                    /*PERSEN BKPU*/
                    
                    /*Hitung Proyek*/
                    $hitung_proyek = count($data[$v]['total_awal_rab']);
                    /*Hitung Proyek*/
                    
                    $dt_nasional['persen_jumlah_awal'][$offset] = $total_persen($data[$v]['persen_jumlah_awal'],$hitung_proyek,true);
                    $dt_nasional['persen_jumlah_real_lalu'][$offset] = $total_persen($data[$v]['persen_jumlah_real_lalu'],$hitung_proyek,true);
                    $dt_nasional['persen_jumlah_real_ini'][$offset] = $total_persen($data[$v]['persen_jumlah_real_ini'],$hitung_proyek,true);
                    $dt_nasional['persen_jumlah_real'][$offset] = $total_persen($data[$v]['persen_jumlah_real'],$hitung_proyek,true);
                    $dt_nasional['persen_jumlah_proyeksi'][$offset] = $total_persen($data[$v]['persen_jumlah_proyeksi'],$hitung_proyek,true);
                    /*PERSEN BKPU*/
                    $offset++;
                }
                
                if($of == "datatable")
                {
                    $value = array(
                        array(
                            'total_awal_rab'=> $total_format($dt_nasional['total_awal_rab']), //1
                            'total_real_rab_lalu'=> $total_format($dt_nasional['total_real_rab_lalu']), //2
                            'total_extern_rab'=> $total_format($dt_nasional['total_extern_rab']), //3
                            'total_real_rab_ini'=> $total_format($dt_nasional['total_real_rab_ini']), //4
                            'total_extern_rab_ini'=> $total_format($dt_nasional['total_extern_rab_ini']), //5
                            'total_real_rab'=> $total_format($dt_nasional['total_real_rab']), //6
                            'total_extern_rab'=> $total_format($dt_nasional['total_extern_rab']), //7
                            'total_sisa_rab'=> $total_format($dt_nasional['total_sisa_rab']), //8
                            'total_proyeksi_rab'=> $total_format($dt_nasional['total_proyeksi_rab']), //9
                            'persen_jumlah_awal'=> $total_persen($dt_nasional['persen_jumlah_awal'],$count_unit), //10
                            'persen_jumlah_real_lalu'=> $total_persen($dt_nasional['persen_jumlah_real_lalu'],$count_unit), //11
                            'persen_jumlah_real_ini'=> $total_persen($dt_nasional['persen_jumlah_real_ini'],$count_unit), //12
                            'persen_jumlah_real'=> $total_persen($dt_nasional['persen_jumlah_real'],$count_unit), //13
                            'persen_jumlah_proyeksi'=> $total_persen($dt_nasional['persen_jumlah_proyeksi'],$count_unit), //14
                        )
                    );
                    
                    /*debugger($dt_nasional['persen_jumlah_awal']);
                    debugger($value);
                    die();*/
                }
                else if($of == 'graphic')
                {
                    $graph_name = array(
                        "total_awal_rab"=>"Nilai Kontrak",
                        "total_real_rab_ini"=>"Realisasi Aktual",
                        "total_extern_rab_ini"=>"Realisasi Diakui",
                        "total_sisa_rab"=>"Sisa Pekerjaan",
                        "total_proyeksi_rab"=>"Proyeksi",
                        "rencana_laba"=>"Rencana (Laba)",
                        "realisasi_laba"=>"Realisasi (Laba)",
                        "proyeksi_laba"=>"Proyeksi (Laba)"
                    );
                    
                    foreach($graph_name as $k=>$v)
                    {
                        $value[] = array(
                            "name"=>$v,
                            "data"=> array(
                                $total_format($dt_nasional[$k],true)
                            )
                        );
                    }
                    
                    /*$value = array(
                        array(
                            "name"=>"Rencana Awal",
                            "data"=> array($total_format($dt_nasional['total_awal_rab'],true))
                        )
                    );*/
                }
                else{
                    die('Invalid Parameter');
                }
                
                
                echo json_encode(array("data"=>$value));
            }
            else if($tipe == 'data_wilayah')
            {
                $offset =0;
                foreach($keys as $v)
                {
                    $dt_wilayah[$offset]['unit_kerja'] = " ".$v;
                    $dt_wilayah[$offset]['total_awal_rab'] = $number_format(array_sum($data[$v]['total_awal_rab']));
                    $dt_wilayah[$offset]['total_real_rab_lalu'] = $number_format(array_sum($data[$v]['total_real_rab_lalu']));
                    $dt_wilayah[$offset]['total_extern_rab'] = $number_format(array_sum($data[$v]['total_extern_rab']));
                    $dt_wilayah[$offset]['total_real_rab_ini'] = $number_format(array_sum($data[$v]['total_real_rab_ini']));
                    $dt_wilayah[$offset]['total_extern_rab_ini'] = $number_format(array_sum($data[$v]['total_extern_rab_ini']));
                    $dt_wilayah[$offset]['total_real_rab'] = $number_format(array_sum($data[$v]['total_real_rab']));
                    $dt_wilayah[$offset]['total_extern_rab'] = $number_format(array_sum($data[$v]['total_extern_rab']));
                    $dt_wilayah[$offset]['total_sisa_rab'] = $number_format(array_sum($data[$v]['total_sisa_rab']));
                    $dt_wilayah[$offset]['total_proyeksi_rab'] = $number_format(array_sum($data[$v]['total_proyeksi_rab']));
                    $dt_wilayah[$offset]['jumlah_awal'] = $number_format(array_sum($data[$v]['jumlah_awal']));
                    $dt_wilayah[$offset]['jumlah_real_lalu'] = $number_format(array_sum($data[$v]['jumlah_real_lalu']));
                    $dt_wilayah[$offset]['jumlah_real_ini'] = $number_format(array_sum($data[$v]['jumlah_real_ini']));
                    $dt_wilayah[$offset]['jumlah_real'] = $number_format(array_sum($data[$v]['jumlah_real']));
                    $dt_wilayah[$offset]['jumlah_proyeksi'] = $number_format(array_sum($data[$v]['jumlah_proyeksi']));
                    
                    /*LABA*/
                    $dt_wilayah[$offset]['rencana_laba'] = array_sum($data[$v]['total_awal_rab'])-array_sum($data[$v]['jumlah_awal']);
                    $dt_wilayah[$offset]['realisasi_laba'] = array_sum($data[$v]['total_real_rab'])-array_sum($data[$v]['jumlah_real']);
                    $dt_wilayah[$offset]['proyeksi_laba'] = array_sum($data[$v]['total_proyeksi_rab'])-array_sum($data[$v]['jumlah_proyeksi']);
                    /*LABA*/
                    
                    
                    /*Hitung Proyek*/
                    $hitung_proyek = count($data[$v]['total_awal_rab']);
                    /*Hitung Proyek*/
                    
                    /*PERSEN*/
                    $dt_wilayah[$offset]['persen_jumlah_awal'] = $total_persen($data[$v]['persen_jumlah_awal'],$hitung_proyek,true);
                    $dt_wilayah[$offset]['persen_jumlah_real_lalu'] = $total_persen($data[$v]['persen_jumlah_real_lalu'],$hitung_proyek,true);
                    $dt_wilayah[$offset]['persen_jumlah_real_ini'] = $total_persen($data[$v]['persen_jumlah_real_ini'],$hitung_proyek,true);
                    $dt_wilayah[$offset]['persen_jumlah_real'] = $total_persen($data[$v]['persen_jumlah_real'],$hitung_proyek,true);
                    $dt_wilayah[$offset]['persen_jumlah_proyeksi'] = $total_persen($data[$v]['persen_jumlah_proyeksi'],$hitung_proyek,true);
                    /*PERSEN*/
                    
                    /*TOTAL*/
                    $total_awal_rab[$offset] = array_sum($data[$v]['total_awal_rab']);
                    $total_real_rab_lalu[$offset] = array_sum($data[$v]['total_real_rab_lalu']);
                    $total_extern_rab[$offset] = array_sum($data[$v]['total_extern_rab']);
                    $total_real_rab_ini[$offset] = array_sum($data[$v]['total_real_rab_ini']);
                    $total_extern_rab_ini[$offset] = array_sum($data[$v]['total_extern_rab_ini']);
                    $total_real_rab[$offset] = array_sum($data[$v]['total_real_rab']);
                    $total_extern_rab[$offset] = array_sum($data[$v]['total_extern_rab']);
                    $total_sisa_rab[$offset] = array_sum($data[$v]['total_sisa_rab']);
                    $total_proyeksi_rab[$offset] = array_sum($data[$v]['total_proyeksi_rab']);
                    $jumlah_awal[$offset] = array_sum($data[$v]['jumlah_awal']);
                    $jumlah_real_lalu[$offset] = array_sum($data[$v]['jumlah_real_lalu']);
                    $jumlah_real_ini[$offset] = array_sum($data[$v]['jumlah_real_ini']);
                    $jumlah_real[$offset] = array_sum($data[$v]['jumlah_real']);
                    $jumlah_proyeksi[$offset] = array_sum($data[$v]['jumlah_proyeksi']);
                    /*TOTAL*/
                    
                    /*TOTAL PERSEN*/
                    $total_persen_jumlah_awal[$offset] = $total_persen($data[$v]['persen_jumlah_awal'],$hitung_proyek,true);
                    $total_persen_jumlah_real_lalu[$offset] = $total_persen($data[$v]['persen_jumlah_real_lalu'],$hitung_proyek,true);
                    $total_persen_jumlah_real_ini[$offset] = $total_persen($data[$v]['persen_jumlah_real_ini'],$hitung_proyek,true);
                    $total_persen_jumlah_real[$offset] = $total_persen($data[$v]['persen_jumlah_real'],$hitung_proyek,true);
                    $total_persen_jumlah_proyeksi[$offset] = $total_persen($data[$v]['persen_jumlah_proyeksi'],$hitung_proyek,true);
                    /*TOTAL PERSEN*/
                    
                    //$test[$offset] = $data[$v]['persen_jumlah_awal'];
                    
                    $offset++;
                }
                
                $of = $this->input->get('of');
                
                if($of == "datatable")
                {
                
                    /*MEMBUAT TOTAL*/
                    $hitung = count($dt_wilayah);
                    $dt_wilayah[$hitung]['unit_kerja'] = "<b>Total</b>";
                    $dt_wilayah[$hitung]['total_awal_rab'] = $total_format($total_awal_rab);
                    $dt_wilayah[$hitung]['total_real_rab_lalu'] = $total_format($total_real_rab_lalu);
                    $dt_wilayah[$hitung]['total_extern_rab'] = $total_format($total_extern_rab);
                    $dt_wilayah[$hitung]['total_real_rab_ini'] = $total_format($total_real_rab_ini);
                    $dt_wilayah[$hitung]['total_extern_rab_ini'] = $total_format($total_extern_rab_ini);
                    $dt_wilayah[$hitung]['total_real_rab'] = $total_format($total_real_rab);
                    $dt_wilayah[$hitung]['total_extern_rab'] = $total_format($total_extern_rab);
                    $dt_wilayah[$hitung]['total_sisa_rab'] = $total_format($total_sisa_rab);
                    $dt_wilayah[$hitung]['total_proyeksi_rab'] = $total_format($total_proyeksi_rab);
                    $dt_wilayah[$hitung]['jumlah_awal'] = $total_format($jumlah_awal);
                    $dt_wilayah[$hitung]['jumlah_real_lalu'] = $total_format($jumlah_real_lalu);
                    $dt_wilayah[$hitung]['jumlah_real_ini'] = $total_format($jumlah_real_ini);
                    $dt_wilayah[$hitung]['jumlah_real'] = $total_format($jumlah_real);
                    $dt_wilayah[$hitung]['jumlah_proyeksi'] = $total_format($jumlah_proyeksi);
                    
                    $dt_wilayah[$hitung]['persen_jumlah_awal'] = $total_persen($total_persen_jumlah_awal,$count_unit);
                    $dt_wilayah[$hitung]['persen_jumlah_real_lalu'] = $total_persen($total_persen_jumlah_real_lalu,$count_unit);
                    $dt_wilayah[$hitung]['persen_jumlah_real_ini'] = $total_persen($total_persen_jumlah_real_ini,$count_unit);
                    $dt_wilayah[$hitung]['persen_jumlah_real'] = $total_persen($total_persen_jumlah_real,$count_unit);
                    $dt_wilayah[$hitung]['persen_jumlah_proyeksi'] = $total_persen($total_persen_jumlah_proyeksi,$count_unit);
                    
                    /*foreach($test as $v){
                        foreach($v as $val)
                        {
                            $testing[] = $val;
                        }
                    }
                    
                    debugger(array_sum($testing));
                    die();*/
                    echo json_encode(array("data"=>$dt_wilayah));
                }
                else if($of == 'graphic')
                {
                    $graph_name = array(
                        "total_awal_rab"=>"Nilai Kontrak",
                        "total_real_rab_ini"=>"Realisasi Aktual",
                        "total_extern_rab_ini"=>"Realisasi Diakui",
                        "total_sisa_rab"=>"Sisa Pekerjaan",
                        "total_proyeksi_rab"=>"Proyeksi",
                        "rencana_laba"=>"Rencana (Laba)",
                        "realisasi_laba"=>"Realisasi (Laba)",
                        "proyeksi_laba"=>"Proyeksi (Laba)",
                    );
                    
                    $stack_name = array(
                        "total_awal_rab"=>"a",
                        "total_real_rab_ini"=>"b",
                        "total_extern_rab_ini"=>"c",
                        "total_sisa_rab"=>"b",
                        "total_proyeksi_rab"=>"d",
                        "rencana_laba"=>"e",
                        "realisasi_laba"=>"f",
                        "proyeksi_laba"=>"g",
                    );
                    
                    //Contoh JSON Object Untuk Developer Selanjutnya
                    /*$test = array(
                        array(
                            "name"=> "Rencana Awals",
                            "data"=>array(
                                3,1,2,3,4,5,6
                            ),
                            "stack"=> "a"
                        ),
                        array(
                            "name"=> "Rencana Awal",
                            "data"=>array(
                                3,1,2,3,4,5,6
                            ),
                            "stack"=> "b"
                        ),
                        array(
                            "name"=> "Rencana Awal 2",
                            "data"=>array(
                                5,6,7,8,9,10,11
                            ),
                            "stack"=> "b"
                        ),
                    );*/
                    
                    foreach($dt_wilayah as $k=>$v)
                    {
                        foreach($v as $key=>$val)
                        {
                            $value[$key][] = $val;
                        }
                    }
                    
                    $offset=0;
                    foreach($graph_name as $k=>$v)
                    {
                        $series[$offset]['name'] = $v;
                        $series[$offset]['data'] = $value[$k];
                        $series[$offset]['stack'] = $stack_name[$k];
                        $offset++;
                    }
                    
                    //debugger($series);
                    
                    
                    //debugger($a);
                    /*$offset =0;
                    foreach($dt_wilayah as $k=>$v)
                    {
                       debugger(array_keys($v));
                    }*/
                    
                    
                    echo json_encode(array("series"=>$series,"categories"=>$keys));
                }
                else{
                    die();
                }
                
               
                //debugger($dt_wilayah);
            }
            else{
                debugger($output);
            }
        }
        catch (Exception $e)
        {
            $this->output->set_content_type('application/json')->set_output($e->getMessage());
        }
    }
    
}
