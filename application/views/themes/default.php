<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo $title; ?></title>
		<meta name="resource-type" content="document" />
		<meta name="robots" content="index, follow"/>
		<meta name="googlebot" content="all, index, follow" />
		<meta name="author" content="Bondan Eko Prasetyo"/>
		<!--<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/default/images/favicon.png" type="image/x-icon"/>-->
       
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/AdminLTE.min.css">
        <!-- Custom style-->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/skins/_all-skins.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datatables/responsive.bootstrap.min.css">
        <?php
        /** -- Copy from here -- */
        foreach($css as $file){
            echo "\n\t\t";
            ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
        } echo "\n\t";
        /** -- to here -- */
        ?>
    </head>

    <body class="skin-blue layout-top-nav">
        
        <div class="wrapper">

            <header class="main-header">
                <nav class="navbar navbar-static-top">
                  <div class="container-fluid">
                    <div class="navbar-header">
                      <a href="#" class="navbar-brand">PT.Nindya Karya (Persero) </a>
                      <button data-target="#navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-bars"></i>
                      </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div id="navbar-collapse" class="collapse navbar-collapse pull-right">
                      <ul class="nav navbar-nav">
                        <li>
                            <a href="javascript:void(0)">
                                Auto Refresh &nbsp; <input type="checkbox" class="autorefresh" onchange="autorefresh(this)" checked/>
                            </a>
                        </li>
                        <li><a href="../">Main Menu <span class="sr-only">(current)</span></a></li>
                        <li><a href="../sign_out.php">Logout</a></li>
                        <!--<li class="dropdown">
                          <a data-toggle="dropdown" class="dropdown-toggle" href="#">Dropdown <span class="caret"></span></a>
                          <ul role="menu" class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                          </ul>
                        </li>-->
                      </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                    <!-- Navbar Right Menu -->
                    <!--<div class="navbar-custom-menu">
                      <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu active hidden-xs hidden-sm">
                          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="hidden-xs">Welcome, Alexander Pierce</span>
                          </a>
                          <ul class="dropdown-menu">
                            <li class="user-header">
                              <img alt="User Image" class="img-circle" src="<?php echo base_url() ?>assets/img/user2-160x160.jpg">

                              <p>
                                Alexander Pierce - Web Developer
                                <small>Member since Nov. 2012</small>
                              </p>
                            </li>
                            <li class="user-footer">
                              <div class="pull-left">
                                <a class="btn btn-default btn-flat" href="#">Profile</a>
                              </div>
                              <div class="pull-right">
                                <a class="btn btn-default btn-flat" href="#">Sign out</a>
                              </div>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>-->
                    <!-- /.navbar-custom-menu -->
                  </div>
                  <!-- /.container-fluid -->
                </nav>
              </header>
            <!-- Left side column. contains the logo and sidebar -->
           

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $output;?>
            <!-- Content Header (Page header) -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> <?php echo $version ?>
                </div>
                <strong>Copyright &copy; 2016 <a href="http://www.nindyakarya.co.id">PT. Nindya Karya (Persero)</a>.</strong> All rights reserved.
            </footer>        
        </div>
        <!-- /.wrapper -->
        <script>
        var link = '<?php echo base_url(); ?>';
        </script>
        <!-- jQuery 2.2.0 -->
        <script src="<?php echo base_url()?>assets/plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url()?>assets/plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url()?>assets/js/app.min.js"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url()?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="<?php echo base_url()?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="<?php echo base_url()?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- ChartJS 1.0.1 -->
        <script src="<?php echo base_url()?>assets/plugins/chartjs/Chart.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!--<script src="<?php echo base_url()?>assets/js/pages/dashboard2.js"></script>-->
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url()?>assets/js/demo.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/highcharts/highcharts.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/highcharts/exporting.js"></script>
        <script src="<?php echo base_url() ?>assets/js/charts.js"> </script>
        <script src="<?php echo base_url() ?>assets/js/datatable.js"> </script>
        <script>
            /*$(document).ajaxStart(function(){
                console.log('start')
            })
            
            $( document ).ajaxComplete(function( event,request, settings ) {
              console.log(settings)
            });*/
            
            function getRandomInt(min, max) {
              min = Math.ceil(min);
              max = Math.floor(max);
              return Math.floor(Math.random() * (max - min)) + min;
            }

            function dummy()
            {
                var angka = getRandomInt(6000000000,120000000000);
                grafik.series[0].setData([angka,angka,angka]);
                grafik.series[1].setData([angka,angka,angka]);
                grafik.series[2].setData([angka,angka,angka]);
            }
            
            function dummy2()
            {
                var angka = getRandomInt(6000000000,120000000000);
                grafik2.series[0].setData([angka,angka,angka]);
                grafik2.series[1].setData([angka,angka,angka]);
                grafik2.series[2].setData([angka,angka,angka]);
            }
            
            function dummy3()
            {
                var angka = getRandomInt(6000000000,120000000000);
                grafik3.series[0].setData([angka]);
                grafik3.series[1].setData([angka]);
                grafik3.series[2].setData([angka]);
            }
            
            var refreshstate = true;
            
            function autorefresh(v){
                if($(v).is(':checked'))
                {
                    refreshstate = true;
                }
                else{
                    refreshstate = false;
                }
            }
            
            /*
            *
            * Fungsi Refresh Untuk Datatable & Graph (Nasioal & Wilayah)
            *
            */
            function refresh()
            {
                try{
                    if(refreshstate == false)
                    {
                        throw "autorefresh_off";
                    }
                    
                    $('#loading_nasional').text('Loading...').fadeIn("fast",function(){
                        $('#loading_nasional').removeClass('hide');
                    });
                    
                    $('#loading_wil').text('Loading...').fadeIn("fast",function(){
                        $('#loading_wil').removeClass('hide');
                    });
                    
                    $.getJSON(link+'welcome/source/data_nasional?of=graphic',function(data){
                        $.each(data.data,function(k,v){
                            graphic_nasional.series[k].setData(v.data);
                        })
                        
                        datatable_nasional.ajax.url(link+'welcome/source/data_nasional?of=datatable').load(function(){
                            $('#loading_nasional').text('Loading Success!').fadeOut("slow",function(){
                                $(this).addClass('hide');
                            });
                        });
                        
                    })
                    
                    $.getJSON(link+'welcome/source/data_wilayah?of=graphic',function(data){
                        $.each(data.series,function(k,v){
                            graphic_wilayah.series[k].setData(v.data);
                        })
                        
                        datatable_wilayah.ajax.url(link+'welcome/source/data_wilayah?of=datatable').load(function(){
                            $('#loading_wil').text('Loading Success!').fadeOut("slow",function(){
                                $(this).addClass('hide');
                            });
                        });
                    })
                }
                catch(e)
                {
                    console.log(e);
                }
            }
            
            /*
            *
            * Fungsi Refresh Untuk Datatable & Graph (Per-Proyek)
            * Berfungsi untuk merefresh unit_kerja dan periode
            *
            */
            function refresh_wil(value,tipe)
            {
                if(tipe == 'prd')
                {
                    var periode = value;
                    var div = $('select[name="divisi"]').val();
                }
                else{
                    var periode = $('select[name="periode"]').val();
                    var div = value;
                }
                
                /*$.getJSON(link+'welcome/source_per_div/'+div+'/'+periode+'?graphic=true',function(data){
                    if(data.status == false)
                    {
                        datatable_proyek.clear().draw();
                    }
                    else{
                        datatable_proyek.ajax.url(link+'welcome/source_per_div/'+div+'/'+periode).load();
                    }
                })*/
                
                // load chart
                try{
                    if(graphic_proyek.series.length == 0)
                    {
                        var refresh = div+","+periode;
                        throw refresh;
                    }
                    else{
                        $('#loading_proyek').text('Loading...').fadeIn("fast",function(){
                            $('#loading_proyek').removeClass('hide');
                        });
                        
                        $.getJSON(link+'welcome/source_per_div/'+div+'/'+periode+'?graphic=true',function(data){
                            if(data.status == false)
                            {
                                var count = Object.keys(graphic_proyek.series).length;
                                for(var i=0;i<count;i++)
                                {
                                     graphic_proyek.series[i].setData([0]);
                                }
                                
                                //alert('Data Belum Tersedia')
                                
                                datatable_proyek.clear().draw();
                                
                                $('#loading_proyek').text(data.msg).fadeOut("slow",function(){
                                    $(this).addClass('hide');
                                });
                            }
                            else{
                                $.each(data.series,function(k,v){
                                    graphic_proyek.series[k].setData(v.data);
                                });
                                //load datatable
                                datatable_proyek.ajax.url(link+'welcome/source_per_div/'+div+'/'+periode).load(function(){
                                    $('#loading_proyek').text('Loading Success!').fadeOut("slow",function(){
                                        $(this).addClass('hide');
                                    });
                                })
                            }
                        })
                    }
                }
                catch(e)
                {
                    var param = e.split(',');
                    var periode = param[1];
                    var div = param[0];
                    
                    $.getJSON(link+'welcome/source_per_div/'+div+'/'+periode+'?graphic=true',function(data){
                        option_proyek.series = data.series;
                        graphic_proyek = new Highcharts.Chart(option_proyek);
                    });
                    
                    datatable_proyek.ajax.url(link+'welcome/source_per_div/'+div+'/'+periode).load();
                }
            }
            
            function change_periode(value,tipe)
            {
                $('#loading_'+tipe).text('Loading...').fadeIn("fast",function(){
                    $('#loading_'+tipe).removeClass('hide');
                });
                
               if(tipe == 'nasional')
               {
                   try {
                        if(graphic_nasional.series.length == 0)
                        {
                            throw value;
                        }
                        else{
                             $.getJSON(link+'welcome/source/data_nasional?of=graphic&periode_bulan='+value,function(data){
                                if(data.data == undefined)
                                {
                                    alert('Data di periode ini tidak tersedia..')
                                }
                                else{
                                    $.each(data.data,function(k,v){
                                        graphic_nasional.series[k].setData(v.data);
                                    })
                                }
                             })
                        }
                    }catch(err)
                    {
                        $.getJSON(link+'welcome/source/data_nasional?of=graphic&periode_bulan='+err,function(data){
                            option_nasional.series = data.data;
                            graphic_nasional = new Highcharts.Chart(option_nasional);
                        })
                    }
                    
                    datatable_nasional.ajax.url(link+'welcome/source/data_nasional?of=datatable&periode_bulan='+value).load(function(){
                            $('#loading_'+tipe).text('Loading Success!').fadeOut("slow",function(){
                                $(this).addClass('hide');
                            });
                    });
               }
               else{
                 try{
                     if(graphic_wilayah.series.length == 0)
                     {
                        throw value;
                     }
                     else{
                         $.getJSON(link+'welcome/source/data_wilayah?of=graphic&periode_bulan='+value,function(data){
                             
                            if(data.series == undefined)
                            {
                                alert('Data di periode ini tidak tersedia..')
                            }
                            else{
                                $.each(data.series,function(k,v){
                                    graphic_wilayah.series[k].setData(v.data);
                                })
                                
                                $.each(data.categories,function(k,v){
                                    if(graphic_wilayah.xAxis[k] != undefined){
                                        graphic_wilayah.xAxis[k].setCategories(data.categories);
                                    }
                                })
                            }
                         })
                     }
                 }
                catch(err)
                 {
                     console.log(err);
                     $.getJSON(link+'welcome/source/data_wilayah?of=graphic&periode_bulan='+err,function(data){
                        options.series = data.series;
                        options.xAxis.categories = data.categories;
                        grafik = new Highcharts.Chart(options);
                    })
                 }
                 
                 datatable_wilayah.ajax.url(link+'welcome/source/data_wilayah?of=datatable&periode_bulan='+value).load(function(){
                     $('#loading_'+tipe).text('Loading Success!').fadeOut("slow",function(){
                        $(this).addClass('hide');
                    });
                 });
               }
            }

            $('.refresh').click(function(){
                refresh();
            })
            
            var ms = parseInt(1000*60); //1 menit
            var menit = <?php echo $menit ?>;
            var reload_time = parseInt(menit*ms);
            
            function change_dropdown_auto()
            {
                var opt = $('.selectbox').find('option');
                var select = opt.filter(':selected')[0];
                var index = parseInt(select.index+1);
                var value;
                //clear selected
                
                try{
                    if(refreshstate == false)
                    {
                        throw "autorefresh_off";
                    }

                    $('.selectbox option').removeAttr('selected');
                    (index < 10)?value = opt[index].value:value = "ALL";

                    $('.selectbox').val(value);
                    refresh_wil(value,'div');
                }
                catch(e)
                {
                    console.log(e);
                }
            }
            //change_dropdown_auto();
            setInterval('refresh()',reload_time);
            setInterval('change_dropdown_auto()',reload_time);
        </script>
        <?php
            foreach($js as $file){
            echo "\n\t\t";
            ?><script src="<?php echo $file; ?>"></script><?php
            } echo "\n\t";
        ?>
    </body>
</html>
