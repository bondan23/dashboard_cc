<section class="content-header">
    <h1>
        Dashboard
        <small>Version <?php echo $version ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
       <div class="col-md-12" id="nasional">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Graphic &amp; Data Nasional</h3> <small class="hide" id="loading_nasional"></small>
                    <div class="pull-right" style="margin-right:20px">
                    PERIODE : 
                    <?php
                        echo form_dropdown('periode',$dropdown_periode,$dropdown_selected,'onchange="change_periode(this.value,\'nasional\')"');
                    ?>
                    </div>
                    <div class="box-tools pull-right">
                        <!--<button class="btn btn-box-tool refresh" type="button"><i class="fa fa-refresh"></i></button>-->
                        <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12" id="gr_nasional" style="width:100%;margin:0 auto;"></div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="table-responsive">
                        <table id="dt_nasional" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="4">Unit Kerja</th>
                                    <th colspan="9">Pendapatan Usaha</th>
                                    <th colspan="5">BK/PU</th>
                                </tr>
                                <tr>
                                    <th rowspan="3">Nilai Kontrak</th>
                                    <th colspan="6">Realisasi</th>
                                    <th rowspan="3">Sisa Pekerjaan</th>
                                    <th rowspan="3">Proyeksi s/d Selesai</th>
                                    <!--BKPU-->
                                    <th rowspan="3">RBK Penetapan</th>
                                    <th colspan="3">Realisasi</th>
                                    <th rowspan="3">Proyeksi s/d Selesai</th>
                                    <!--BKPU-->
                                </tr>
                                <tr>
                                    <th colspan="2">S/d Lalu</th>
                                    <th colspan="2">Kini</th>
                                    <th colspan="2">S/d Kini</th>
                                    
                                    <!--BKPU-->
                                    <th rowspan="2">S/d Lalu</th>
                                    <th rowspan="2">Kini</th>
                                    <th rowspan="2">S/d Kini</th>
                                    <!--BKPU-->
                                </tr>
                                <tr>
                                    <th>Aktual</th>
                                    <th>Diakui</th>
                                    <th>Aktual</th>
                                    <th>Diakui</th>
                                    <th>Aktual</th>
                                    <th>Diakui</th>
                                </tr>
                                <!--<tr>
                                    <th>Rencana Awal</th>
                                    <th>Realisasi Internal</th>
                                    <th>Diakui Owner</th>
                                    <th>Rencana Sisa</th>
                                    <th>Proyeksi</th>
                                    <th>Rencana</th>
                                    <th>Realisasi</th>
                                    <th>Proyeksi</th>
                                    <th>Rencana</th>
                                    <th>Realisasi</th>
                                    <th>Proyeksi</th>
                                </tr>-->
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        
        <div class="col-md-12" id="wilayah">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Graphic &amp; Data Wilayah</h3> <small class="hide" id="loading_wil"></small>
                    <div class="pull-right" style="margin-right:20px">
                    PERIODE : 
                    <?php
                        echo form_dropdown('periode',$dropdown_periode,$dropdown_selected,'onchange="change_periode(this.value,\'wil\')"');
                    ?>
                    </div>
                    <div class="box-tools pull-right">
                        <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12" id="gr_wilayah" style="width:100%;margin:0 auto;"></div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="table-responsive">
                        <table id="dt_wilayah" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="4">Unit Kerja</th>
                                    <th colspan="9">Pendapatan Usaha</th>
                                    <th colspan="5">BK/PU</th>
                                </tr>
                                <tr>
                                    <th rowspan="3">Nilai Kontrak</th>
                                    <th colspan="6">Realisasi</th>
                                    <th rowspan="3">Sisa Pekerjaan</th>
                                    <th rowspan="3">Proyeksi s/d Selesai</th>
                                    <!--BKPU-->
                                    <th rowspan="3">RBK Penetapan</th>
                                    <th colspan="3">Realisasi</th>
                                    <th rowspan="3">Proyeksi s/d Selesai</th>
                                    <!--BKPU-->
                                </tr>
                                <tr>
                                    <th colspan="2">S/d Lalu</th>
                                    <th colspan="2">Kini</th>
                                    <th colspan="2">S/d Kini</th>
                                    
                                    <!--BKPU-->
                                    <th rowspan="2">S/d Lalu</th>
                                    <th rowspan="2">Kini</th>
                                    <th rowspan="2">S/d Kini</th>
                                    <!--BKPU-->
                                </tr>
                                <tr>
                                    <th>Aktual</th>
                                    <th>Diakui</th>
                                    <th>Aktual</th>
                                    <th>Diakui</th>
                                    <th>Aktual</th>
                                    <th>Diakui</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        
        <div class="col-md-12" id="proyek">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Graphic &amp; Data Proyek </h3> <small class="hide" id="loading_proyek"></small>
                    <div class="pull-right" style="margin-right:20px">
                    UNIT KERJA : 
                    <?php
                        echo form_dropdown('divisi',$divisi,'ALL','onchange="refresh_wil(this.value,\'div\')" class="selectbox"');
                    ?>
                    PERIODE : 
                    <?php
                        echo form_dropdown('periode',$dropdown_periode,$dropdown_selected,'onchange="refresh_wil(this.value,\'prd\')"');
                    ?>
                    </div>
                    <div class="box-tools pull-right">
                        <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12" id="gr_proyek" style="width:100%;margin:0 auto;"></div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="table-responsive">
                        <table id="dt_proyek" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="4">No</th>
                                    <th rowspan="4">Unit Kerja</th>
                                    <th rowspan="4">Nama Proyek</th>
                                    <th colspan="9">Pendapatan Usaha</th>
                                    <th colspan="5">BK/PU</th>
                                </tr>
                                <tr>
                                    <th rowspan="3">Nilai Kontrak</th>
                                    <th colspan="6">Realisasi</th>
                                    <th rowspan="3">Sisa Pekerjaan</th>
                                    <th rowspan="3">Proyeksi s/d Selesai</th>
                                    <!--BKPU-->
                                    <th rowspan="3">RBK Penetapan</th>
                                    <th colspan="3">Realisasi</th>
                                    <th rowspan="3">Proyeksi s/d Selesai</th>
                                    <!--BKPU-->
                                </tr>
                                <tr>
                                    <th colspan="2">S/d Lalu</th>
                                    <th colspan="2">Kini</th>
                                    <th colspan="2">S/d Kini</th>
                                    
                                    <!--BKPU-->
                                    <th rowspan="2">S/d Lalu</th>
                                    <th rowspan="2">Kini</th>
                                    <th rowspan="2">S/d Kini</th>
                                    <!--BKPU-->
                                </tr>
                                <tr>
                                    <th>Aktual</th>
                                    <th>Diakui</th>
                                    <th>Aktual</th>
                                    <th>Diakui</th>
                                    <th>Aktual</th>
                                    <th>Diakui</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

        <!--<div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Graphic &amp; Data Nasional</h3>
                    <div class="box-tools pull-right">
                        <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12" id="container2" style="width:100%;margin:0 auto;"></div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="table-responsive">
                        <table id="rekap" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Pendapatan Usaha (Rp.)</th>
                                    <th>Pendapatan Usaha (%)</th>
                                    <th>BK/PU (%)</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>-->
        
    </div>
</section>
<!-- /.content -->